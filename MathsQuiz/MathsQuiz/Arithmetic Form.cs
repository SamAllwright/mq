﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathsQuiz
{
    public partial class frmArithmetic : Form
    {
        public frmArithmetic()
        {
            //initializing 
            //sets combo boxes default values
            //neg or not is set within properties
            InitializeComponent();
            cmbBoxOperations.SelectedIndex = 0;
            cmbBoxMagnitude.SelectedItem = "0-10";
        }

        private void frmArithmetic_Load(object sender, EventArgs e)
        {

        }
        //declarations of variables that are all used in multiple functions throughout program
        //allows for wide scope and flexibility
        int VariableNumberA;
        int VariableNumberB;
        int operationFunctionNum;
        int mathsAnswer;
        int minRange;
        int maxRange;
        string operatorFunc;
        int questionsCorrect = 0;
        int questionsAnswered = 0;
        int seconds = 0;
        int totalSeconds = 0;

        private void operatorSelection()
        {
            //decides a corresponding number code that coordinates to selected text box for operations
            //this allows for shorthand in using operationFunctionNum later on
            if(cmbBoxOperations.Text == "Addition")
            {
                operationFunctionNum = 0;
            }
            else if(cmbBoxOperations.Text == "Subtraction")
            {
                operationFunctionNum = 1;
            }
            else if (cmbBoxOperations.Text == "Multiplication")
            {
                operationFunctionNum = 2;
            }
            else if (cmbBoxOperations.Text == "Division")
            {
                operationFunctionNum = 3;
            }

        }

        private string magnitudeSelection()
        {
            //similar to operatorselection() except returns string instead of int
            // "noSelect logically shouldnt be accessable because there are only the 3 text choices in combobox
            //allows magnitudeselection when called to return a string instead of having a preset variable
            if(cmbBoxMagnitude.Text == "0-10")
            {
                return "10";
            }
            else if (cmbBoxMagnitude.Text == "0-100")
            {
                return "100";
            }
            else if (cmbBoxMagnitude.Text == "0-1000")
            {
                return "1000";
            }
            else
            {
                return "noSelect";
            }
        }

        private string negOrNot()
        {
            //returns string for negative included or not
            //like mag selection noselect shouldnt be accessable unless extra options added on form
            if (rdBtnPosOnly.Checked == true)
            {
                return "positive";
            }
            else if(rdBtnNegAndPos.Checked == true)
            {
                return "negAndPos";
            }
            else
            {
                return "noSelect";
            }
        }

        private int createQuestion()
        {
            //function starts by calling operator function to determine operation of question needed
            //then uses variables set by neg and mag functions called to decide range
            operatorSelection();

            #region magnitude
            if (magnitudeSelection() == "10")
            {
                maxRange = 10;
            }
            else if (magnitudeSelection() == "100")
            {
                maxRange = 100;
            }
            else if (magnitudeSelection() == "1000")
            {
                maxRange = 1000;
            }
            #endregion magnitude

            #region negative Positive
            if (negOrNot() == "positive")
            {
                minRange = 0;
            }
            else if (negOrNot() == "negAndPos")
            {
                minRange = -maxRange;
            }
            else
            {
                MessageBox.Show("Select questions Positive Only or Negative and Positive");
            }
            #endregion negative Positive


            Random rnd = new Random();
            VariableNumberA = rnd.Next(minRange, maxRange);
            VariableNumberB = rnd.Next(minRange, maxRange);

            #region operatorsAndAnswerCreation
            //fairly simple, creates an answer based on calculation done within if statement
            //also assigns a string character to be used to display question
            //returns the answer to mathsanswer to be held there
            if (operationFunctionNum == 0)
            {
                operatorFunc = "+";
                mathsAnswer = VariableNumberA + VariableNumberB;
                return mathsAnswer;
            }
            else if (operationFunctionNum == 1)
            {
                operatorFunc = "-";
                mathsAnswer = VariableNumberA - VariableNumberB;
                return mathsAnswer;
            }
            else if (operationFunctionNum == 2)
            {
                operatorFunc = "*";
                mathsAnswer = VariableNumberA * VariableNumberB;
                return mathsAnswer;
            }
            else if (operationFunctionNum == 3)
            {
                //the division is slightly more tricky, while the isWholenumber function doesnt return true, regenerates numbers
                //once they have generated numbers able to create a whole number, creates an answer from those two variables
                operatorFunc = "/";
                
                while(isWholeNumber() != true)
                {
                    VariableNumberA = rnd.Next(minRange, maxRange);
                    VariableNumberB = rnd.Next(minRange, maxRange);
                }
                mathsAnswer = VariableNumberA / VariableNumberB;
                return mathsAnswer;
            }
            else
            {
                return -1;
            }

            #endregion operatorsAndAnswerCreation
        }

        private void displayQuestion()
        {
            //calls create question() to create all the variables for the actual written question string 
            int mathsAnsw = createQuestion();
            lstVwQuestions.Items.Clear();
            lstVwQuestions.Items.Add("What is " + VariableNumberA + " " + operatorFunc + " " + VariableNumberB + "?");

        }

        private void answeringQuestion()
        {
            //converts textbox answer into integer
            int intAnswer = Convert.ToInt32(txtAnswers.Text);

            //displays the result of the question
            txtCorrectAns.Text = Convert.ToString(mathsAnswer);
            txtCorrectAns.Visible = true;
            lblCorrectAns.Visible = true;
            lblTimeLeft.Visible = false;
            //if both answers are equal then adds to questionscorrect counter
            if (intAnswer == mathsAnswer)
            {
                questionsCorrect++;
            }
        }

        private void launchTable()
        {
            //launchs times table and hides current form
                this.Hide();

                frmTimesTable quiz = new frmTimesTable();
                quiz.Show();

        }

        private bool isNum()
        {
            //all the same logic from times table but replaced input with txtAnswers.text
            int n;
            bool isNumeric = int.TryParse(txtAnswers.Text, out n);
            if (isNumeric == true)
            {
                return true;
            }
            else
            {
                MessageBox.Show("Enter only numbers");
                return false;
            }
        }

        private bool isWholeNumber()
        {
            //this function is used in createquestion() when creating numbers that divided are whole numbers
            //A divided by B, if there is a remainder equal to zero then returns true
            //casts the variablenumbers to doubles just for this function so random gen still makes only whole numbers
            //this had to be put in a try catch because crashes when attempts divide by zero otherwise
            try
            {
                return ((double)VariableNumberA % (double)VariableNumberB == 0);
            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return false;
        }

        private void questionColours(double percentageCorrect)
        {
            //simple, runs the backcolor conversions for two labels depended on questions correct divided by answered
            if (percentageCorrect >= 0.80)
            {
                lblQuestionNumRight.BackColor = Color.LightGreen;
                lblQuestionNumAns.BackColor = Color.LightGreen;
            }
            else if (percentageCorrect >= 0.60 && percentageCorrect< 0.80)
            {
                lblQuestionNumRight.BackColor = Color.Orange;
                lblQuestionNumAns.BackColor = Color.Orange;
            }
            else if (percentageCorrect <0.60)
            {
                lblQuestionNumRight.BackColor = Color.Red;
                lblQuestionNumAns.BackColor = Color.Red;
            }
        }

        private void secondsLeft()
        {
            //simple but long function, decides a variable of total seconds dependent on operation variable and magnitude function
            if (operationFunctionNum == 0 && magnitudeSelection() == "10")
            {
                totalSeconds = 4;
            }
            else if (operationFunctionNum == 1 && magnitudeSelection() == "10")
            {
                totalSeconds = 4;
            }
            else if (operationFunctionNum == 2 && magnitudeSelection() == "10")
            {
                totalSeconds = 8;
            }
            else if (operationFunctionNum == 3 && magnitudeSelection() == "10")
            {
                totalSeconds = 4;
            }
            else if (operationFunctionNum == 0 && magnitudeSelection() == "100")
            {
                totalSeconds = 12;
            }
            else if (operationFunctionNum == 1 && magnitudeSelection() == "100")
            {
                totalSeconds = 12;
            }
            else if (operationFunctionNum == 2 && magnitudeSelection() == "100")
            {
                totalSeconds = 90;
            }
            else if (operationFunctionNum == 3 && magnitudeSelection() == "100")
            {
                totalSeconds = 12;
            }
            else if (operationFunctionNum == 0 && magnitudeSelection() == "1000")
            {
                totalSeconds = 30;
            }
            else if (operationFunctionNum == 1 && magnitudeSelection() == "1000")
            {
                totalSeconds = 30;
            }
            else if (operationFunctionNum == 2 && magnitudeSelection() == "1000")
            {
                totalSeconds = 210;
            }
            else if (operationFunctionNum == 3 && magnitudeSelection() == "1000")
            {
                totalSeconds = 45;
            }
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            //sets answer button,end session and text box for answers to visible once question generated
            //correct answer and session details not visible
            //also clears answer box of previous and calls display function to show next question
            btnEndSession.Visible = true;
            btnAnswer.Visible = true;
            txtAnswers.Visible = true;
            lblAnswer.Visible = true;
            txtCorrectAns.Visible = false;
            lblCorrectAns.Visible = false;
            txtAnswers.Clear();
            displayQuestion();
            lblQuestionNumAns.Visible = false;
            lblQuestionNumRight.Visible = false;

        }

        private void btnAnswer_Click(object sender, EventArgs e)
        {
            //tests to see a number has been entered into answer box calling isNum
            //if valid stops timer, calls answering question function, adds to timer and sets visible flags to false
            if(isNum() == true)
            {
                tmrClockMode.Stop();

                answeringQuestion();
                questionsAnswered++;
                btnAnswer.Visible = false;
                txtAnswers.Visible = false;
                lblAnswer.Visible = false;
            }
        }

        private void btnLaunchTable_Click(object sender, EventArgs e)
        {
            launchTable();
        }

        private void btnEndSession_Click(object sender, EventArgs e)
        {
            double percentageCorrect;
            //creates percentage correct out of question statistics
            //inputs that number into question colours and calls it
            //has to be in try to prevent divided by zero crash
            //writes out attempted and right and resets for new session
            try
            {
                percentageCorrect = (double)questionsCorrect / (double)questionsAnswered;
                questionColours(percentageCorrect);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            lblQuestionNumAns.Text = "Questions Attempted: " + questionsAnswered.ToString();
            lblQuestionNumRight.Text = "Questions Right: " + questionsCorrect.ToString();
            lblQuestionNumAns.Visible = true;
            lblQuestionNumRight.Visible = true;
            questionsAnswered = 0;
            questionsCorrect = 0;

            tmrClockMode.Stop();
            lblTimeLeft.Visible = false;
            btnEndSession.Visible = false;

        }

        private void btnClockMode_Click(object sender, EventArgs e)
        {
            //sets visibility states of different objects on the form for special against the clock mode
            //also resets values before starting up clock mode on click
            btnClockMode.Visible = false;
            btnEndSession.Visible = true;
            tmrClockMode.Stop();
            seconds = 0;

            btnAnswer.Visible = true;
            txtAnswers.Visible = true;
            lblAnswer.Visible = true;
            txtCorrectAns.Visible = false;
            lblCorrectAns.Visible = false;
            lblQuestionNumAns.Visible = false;
            lblQuestionNumRight.Visible = false;
            txtAnswers.Clear();
            displayQuestion();

            lblTimeLeft.Visible = true;

            secondsLeft();
            tmrClockMode.Start();
        }

        private void tmrClockMode_Tick(object sender, EventArgs e)
        {
            //every tick increment seconds and update string to show amount of seconds left
            //when seconds left = 0 change states of visibility and increase answered questions
            seconds++;
            lblTimeLeft.Text = "Time Left: " + Convert.ToString(totalSeconds - seconds) + " Seconds";

            if(totalSeconds-seconds == 0)
            {
                tmrClockMode.Stop();
                btnAnswer.Visible = false;
                lblAnswer.Visible = false;
                txtAnswers.Visible = false;
                txtCorrectAns.Visible = true;
                lblCorrectAns.Visible = true;
                btnClockMode.Visible = true;
                lblTimeLeft.Visible = false;
                txtCorrectAns.Text = mathsAnswer.ToString();
                questionsAnswered++;
            }
        }
    }
}
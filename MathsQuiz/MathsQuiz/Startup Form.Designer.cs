﻿namespace MathsQuiz
{
    partial class frmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartup));
            this.picBoxCycle = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnLaunchQuiz = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCycle)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxCycle
            // 
            this.picBoxCycle.InitialImage = null;
            this.picBoxCycle.Location = new System.Drawing.Point(208, 217);
            this.picBoxCycle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picBoxCycle.Name = "picBoxCycle";
            this.picBoxCycle.Size = new System.Drawing.Size(324, 175);
            this.picBoxCycle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picBoxCycle.TabIndex = 0;
            this.picBoxCycle.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnLaunchQuiz
            // 
            this.btnLaunchQuiz.Location = new System.Drawing.Point(324, 542);
            this.btnLaunchQuiz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLaunchQuiz.Name = "btnLaunchQuiz";
            this.btnLaunchQuiz.Size = new System.Drawing.Size(213, 126);
            this.btnLaunchQuiz.TabIndex = 1;
            this.btnLaunchQuiz.Text = "Launch Quiz";
            this.btnLaunchQuiz.UseVisualStyleBackColor = true;
            this.btnLaunchQuiz.Click += new System.EventHandler(this.btnLaunchQuiz_Click);
            // 
            // frmStartup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 957);
            this.Controls.Add(this.btnLaunchQuiz);
            this.Controls.Add(this.picBoxCycle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmStartup";
            this.Text = "Maths As Fun Learning Company Start Up Screen";
            this.Load += new System.EventHandler(this.frmStartup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCycle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxCycle;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnLaunchQuiz;
    }
}


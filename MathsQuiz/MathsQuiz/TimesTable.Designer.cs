﻿namespace MathsQuiz
{
    partial class frmTimesTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTimesTable));
            this.lblGridSize = new System.Windows.Forms.Label();
            this.tblTableGrid = new System.Windows.Forms.TableLayoutPanel();
            this.btnGenTable = new System.Windows.Forms.Button();
            this.txtSizeGrid = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblGridSize
            // 
            this.lblGridSize.AutoSize = true;
            this.lblGridSize.Location = new System.Drawing.Point(258, 170);
            this.lblGridSize.Name = "lblGridSize";
            this.lblGridSize.Size = new System.Drawing.Size(101, 20);
            this.lblGridSize.TabIndex = 2;
            this.lblGridSize.Text = "Size of Grid?";
            // 
            // tblTableGrid
            // 
            this.tblTableGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblTableGrid.AutoScroll = true;
            this.tblTableGrid.AutoSize = true;
            this.tblTableGrid.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tblTableGrid.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblTableGrid.ColumnCount = 1;
            this.tblTableGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblTableGrid.Location = new System.Drawing.Point(262, 306);
            this.tblTableGrid.Name = "tblTableGrid";
            this.tblTableGrid.RowCount = 1;
            this.tblTableGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblTableGrid.Size = new System.Drawing.Size(2, 2);
            this.tblTableGrid.TabIndex = 3;
            // 
            // btnGenTable
            // 
            this.btnGenTable.Location = new System.Drawing.Point(523, 223);
            this.btnGenTable.Name = "btnGenTable";
            this.btnGenTable.Size = new System.Drawing.Size(204, 49);
            this.btnGenTable.TabIndex = 4;
            this.btnGenTable.Text = "Generate Table";
            this.btnGenTable.UseVisualStyleBackColor = true;
            this.btnGenTable.Click += new System.EventHandler(this.btnGenTable_Click);
            // 
            // txtSizeGrid
            // 
            this.txtSizeGrid.Location = new System.Drawing.Point(381, 170);
            this.txtSizeGrid.Name = "txtSizeGrid";
            this.txtSizeGrid.Size = new System.Drawing.Size(100, 26);
            this.txtSizeGrid.TabIndex = 5;
            // 
            // frmTimesTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 961);
            this.Controls.Add(this.txtSizeGrid);
            this.Controls.Add(this.btnGenTable);
            this.Controls.Add(this.tblTableGrid);
            this.Controls.Add(this.lblGridSize);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmTimesTable";
            this.Text = "Maths As Fun Learning Company Times Tables";
            this.Load += new System.EventHandler(this.frmTimesTable_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblGridSize;
        private System.Windows.Forms.TableLayoutPanel tblTableGrid;
        private System.Windows.Forms.Button btnGenTable;
        private System.Windows.Forms.TextBox txtSizeGrid;
    }
}
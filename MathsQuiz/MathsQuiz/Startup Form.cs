﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text; 
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathsQuiz
{
    public partial class frmStartup : Form
    {
        //initial delarations of images and seconds for counter
        Image copyright;
        Image circle;
        Image square;
        Image triangle;
        int seconds = 0;

        public frmStartup()
        {
            InitializeComponent();
        }

        private void frmStartup_Load(object sender, EventArgs e)
        {
            //states where images get pictures from
            copyright = Image.FromFile(@"logo.png");
            circle = Image.FromFile(@"circle.png");
            square = Image.FromFile(@"rect.png");
            triangle = Image.FromFile(@"tri.png");
            //states which image is current
            picBoxCycle.Image = copyright;
        }

        private void launchQuiz()
        {
            //states that as long as image isnt copyright(gone after 15 seconds) then hides current form and opens quiz
            if (picBoxCycle.Image != copyright)
            {
                this.Hide();

                frmArithmetic quiz = new frmArithmetic();
                quiz.Show();
                
            }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            #region shapes
            //states the lengths each image shows
            seconds++;
            if (seconds == 0)
            {
                picBoxCycle.Image = copyright;
            }
            if (seconds == 15)
            {
                picBoxCycle.Image = circle;
            }
            if (seconds == 18)
            {
                picBoxCycle.Image = square;
            }
            if (seconds == 21)
            {
                picBoxCycle.Image = triangle;
            }
            if (seconds == 24)
            {
                seconds = 15;
            }

            #endregion
        }

        private void btnLaunchQuiz_Click(object sender, EventArgs e)
        {
            //calls launchquiz function on click to open next form
            launchQuiz();
        }
    }
}

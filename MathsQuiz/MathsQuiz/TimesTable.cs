﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MathsQuiz
{
    public partial class frmTimesTable : Form
    {

        public frmTimesTable()
        {
            InitializeComponent();
        }

        private void frmTimesTable_Load(object sender, EventArgs e)
        {

        }

        private void generateTable()
        {
            //function for generating the times table, first off resets previous text and clears controls 
            //this is needed incase grid has been generated before
            tblTableGrid.ResetText();
            tblTableGrid.Controls.Clear();

            //gridsize will be +1 because of first yellow columns
            //determines the counts for columns and rows to be whatever value was entered
            //also keeps it invisible while it draws grid
            int gridSize = Convert.ToInt32(txtSizeGrid.Text) + 1;
            tblTableGrid.ColumnCount = gridSize;
            tblTableGrid.RowCount = gridSize;
            tblTableGrid.Visible = false;

            //increments all the rows, and for each row runs the column for loop to increment columns drawn
            for (int row = 0; row < tblTableGrid.RowCount; row++)
            {
                for (int col = 0; col < tblTableGrid.ColumnCount; col++)
                {
                    // assigns x/y value of 1 if col and row == 0, if not assigns value of row and col
                    //this code lets you have the column and row that states whats being timesed,
                    //- and not just have the actual times table values
                    var x = (row == 0) ? 1 : row;
                    var y = (col == 0) ? 1 : col;

                    //adds a label for controls over what goes in each cell of the grid, then states size
                    //this is how the table is populated
                    tblTableGrid.Controls.Add(new Label
                    {
                        Text = ((x) * (y)).ToString(),
                        Size = new Size(40, 20)
                    }, col, row);

                    // this sets the basic bacground colour as white and for top row, first column yellow
                    tblTableGrid.BackColor = Color.White;
                    this.tblTableGrid.GetControlFromPosition(0, row).BackColor = Color.Yellow;
                    this.tblTableGrid.GetControlFromPosition(col, 0).BackColor = Color.Yellow;
                    
                    if(col == row)
                    {
                        //this states if column is the same cell position as row then red and white colour
                        //this makes it diagonal of which numbers times themselves
                        this.tblTableGrid.GetControlFromPosition(col, row).BackColor = Color.Red;
                        this.tblTableGrid.GetControlFromPosition(col, row).ForeColor = Color.White;
                    }

                }
            }
            //finally after drawing table makes it visible
            tblTableGrid.Visible = true;
        }

        private bool inRange()
        {
            //this function tests to see if the gridsize for times table is within range
            //whole values between one and twelve only and if false is returned will show messagebox
            int gridRange = Convert.ToInt32(txtSizeGrid.Text);
            if (gridRange >= 1 && gridRange <= 12)
            {
                return true;
            }
            else
            {
                MessageBox.Show("Enter number between 1 & 12");
                return false;
            }
        }

        private bool isNum()
        {
            //this function tests to make sure that the value entered is a number
            //needed to avoid crashes
            //isNumeric returns true if number has been parsed to be int
            //else messagebox
            int n;
            bool isNumeric = int.TryParse(txtSizeGrid.Text, out n);
            if (isNumeric == true)
            {
                return true;
            }
            else
            {
                MessageBox.Show("Enter only numbers and only wholenumbers");
                return false;
            }
        }

        private void btnGenTable_Click(object sender, EventArgs e)
        {
            //this button click actually generates table and calls all the methods
            if (isNum() == true)
            {
                if (inRange() == true)
                {
                    generateTable();
                }
            }
        }
    }
}

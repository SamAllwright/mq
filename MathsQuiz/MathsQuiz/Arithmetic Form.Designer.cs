﻿namespace MathsQuiz
{
    partial class frmArithmetic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmArithmetic));
            this.cmbBoxOperations = new System.Windows.Forms.ComboBox();
            this.lblOpReq = new System.Windows.Forms.Label();
            this.lblMagReq = new System.Windows.Forms.Label();
            this.cmbBoxMagnitude = new System.Windows.Forms.ComboBox();
            this.rdBtnNegAndPos = new System.Windows.Forms.RadioButton();
            this.rdBtnPosOnly = new System.Windows.Forms.RadioButton();
            this.lstVwQuestions = new System.Windows.Forms.ListView();
            this.txtAnswers = new System.Windows.Forms.TextBox();
            this.lblQuestions = new System.Windows.Forms.Label();
            this.lblAnswer = new System.Windows.Forms.Label();
            this.btnGen = new System.Windows.Forms.Button();
            this.btnAnswer = new System.Windows.Forms.Button();
            this.txtCorrectAns = new System.Windows.Forms.TextBox();
            this.lblCorrectAns = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnLaunchTable = new System.Windows.Forms.Button();
            this.btnEndSession = new System.Windows.Forms.Button();
            this.lblQuestionNumRight = new System.Windows.Forms.Label();
            this.lblQuestionNumAns = new System.Windows.Forms.Label();
            this.btnClockMode = new System.Windows.Forms.Button();
            this.lblTimeLeft = new System.Windows.Forms.Label();
            this.tmrClockMode = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // cmbBoxOperations
            // 
            this.cmbBoxOperations.FormattingEnabled = true;
            this.cmbBoxOperations.Items.AddRange(new object[] {
            "Addition",
            "Subtraction",
            "Multiplication",
            "Division"});
            this.cmbBoxOperations.Location = new System.Drawing.Point(252, 80);
            this.cmbBoxOperations.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbBoxOperations.Name = "cmbBoxOperations";
            this.cmbBoxOperations.Size = new System.Drawing.Size(180, 28);
            this.cmbBoxOperations.TabIndex = 0;
            // 
            // lblOpReq
            // 
            this.lblOpReq.AutoSize = true;
            this.lblOpReq.Location = new System.Drawing.Point(80, 85);
            this.lblOpReq.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOpReq.Name = "lblOpReq";
            this.lblOpReq.Size = new System.Drawing.Size(157, 20);
            this.lblOpReq.TabIndex = 1;
            this.lblOpReq.Text = "Operation Required?";
            // 
            // lblMagReq
            // 
            this.lblMagReq.AutoSize = true;
            this.lblMagReq.Location = new System.Drawing.Point(80, 143);
            this.lblMagReq.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMagReq.Name = "lblMagReq";
            this.lblMagReq.Size = new System.Drawing.Size(162, 20);
            this.lblMagReq.TabIndex = 2;
            this.lblMagReq.Text = "Magnitude Required?";
            // 
            // cmbBoxMagnitude
            // 
            this.cmbBoxMagnitude.FormattingEnabled = true;
            this.cmbBoxMagnitude.Items.AddRange(new object[] {
            "0-10",
            "0-100",
            "0-1000"});
            this.cmbBoxMagnitude.Location = new System.Drawing.Point(252, 138);
            this.cmbBoxMagnitude.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbBoxMagnitude.Name = "cmbBoxMagnitude";
            this.cmbBoxMagnitude.Size = new System.Drawing.Size(180, 28);
            this.cmbBoxMagnitude.TabIndex = 3;
            // 
            // rdBtnNegAndPos
            // 
            this.rdBtnNegAndPos.AutoSize = true;
            this.rdBtnNegAndPos.Location = new System.Drawing.Point(500, 82);
            this.rdBtnNegAndPos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdBtnNegAndPos.Name = "rdBtnNegAndPos";
            this.rdBtnNegAndPos.Size = new System.Drawing.Size(220, 24);
            this.rdBtnNegAndPos.TabIndex = 4;
            this.rdBtnNegAndPos.Text = "Include Negative Numbers";
            this.rdBtnNegAndPos.UseVisualStyleBackColor = true;
            // 
            // rdBtnPosOnly
            // 
            this.rdBtnPosOnly.AutoSize = true;
            this.rdBtnPosOnly.Checked = true;
            this.rdBtnPosOnly.Location = new System.Drawing.Point(500, 140);
            this.rdBtnPosOnly.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdBtnPosOnly.Name = "rdBtnPosOnly";
            this.rdBtnPosOnly.Size = new System.Drawing.Size(259, 24);
            this.rdBtnPosOnly.TabIndex = 5;
            this.rdBtnPosOnly.TabStop = true;
            this.rdBtnPosOnly.Text = "Dont Include Negative Numbers";
            this.rdBtnPosOnly.UseVisualStyleBackColor = true;
            // 
            // lstVwQuestions
            // 
            this.lstVwQuestions.FullRowSelect = true;
            this.lstVwQuestions.Location = new System.Drawing.Point(84, 281);
            this.lstVwQuestions.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstVwQuestions.Name = "lstVwQuestions";
            this.lstVwQuestions.Size = new System.Drawing.Size(394, 148);
            this.lstVwQuestions.TabIndex = 6;
            this.lstVwQuestions.UseCompatibleStateImageBehavior = false;
            this.lstVwQuestions.View = System.Windows.Forms.View.List;
            // 
            // txtAnswers
            // 
            this.txtAnswers.Location = new System.Drawing.Point(503, 281);
            this.txtAnswers.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAnswers.Name = "txtAnswers";
            this.txtAnswers.Size = new System.Drawing.Size(180, 26);
            this.txtAnswers.TabIndex = 7;
            // 
            // lblQuestions
            // 
            this.lblQuestions.AutoSize = true;
            this.lblQuestions.Location = new System.Drawing.Point(80, 246);
            this.lblQuestions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuestions.Name = "lblQuestions";
            this.lblQuestions.Size = new System.Drawing.Size(231, 20);
            this.lblQuestions.TabIndex = 8;
            this.lblQuestions.Text = "Answer the Following Question:";
            // 
            // lblAnswer
            // 
            this.lblAnswer.AutoSize = true;
            this.lblAnswer.Location = new System.Drawing.Point(496, 246);
            this.lblAnswer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnswer.Name = "lblAnswer";
            this.lblAnswer.Size = new System.Drawing.Size(170, 20);
            this.lblAnswer.TabIndex = 9;
            this.lblAnswer.Text = "Type the Answer Here:";
            // 
            // btnGen
            // 
            this.btnGen.Location = new System.Drawing.Point(252, 174);
            this.btnGen.Name = "btnGen";
            this.btnGen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnGen.Size = new System.Drawing.Size(180, 69);
            this.btnGen.TabIndex = 11;
            this.btnGen.Text = "Generate Question";
            this.btnGen.UseVisualStyleBackColor = true;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // btnAnswer
            // 
            this.btnAnswer.Location = new System.Drawing.Point(500, 334);
            this.btnAnswer.Name = "btnAnswer";
            this.btnAnswer.Size = new System.Drawing.Size(179, 68);
            this.btnAnswer.TabIndex = 12;
            this.btnAnswer.Text = "Answer Question";
            this.btnAnswer.UseVisualStyleBackColor = true;
            this.btnAnswer.Click += new System.EventHandler(this.btnAnswer_Click);
            // 
            // txtCorrectAns
            // 
            this.txtCorrectAns.Location = new System.Drawing.Point(503, 475);
            this.txtCorrectAns.Name = "txtCorrectAns";
            this.txtCorrectAns.ReadOnly = true;
            this.txtCorrectAns.Size = new System.Drawing.Size(176, 26);
            this.txtCorrectAns.TabIndex = 13;
            this.txtCorrectAns.Visible = false;
            // 
            // lblCorrectAns
            // 
            this.lblCorrectAns.AutoSize = true;
            this.lblCorrectAns.Location = new System.Drawing.Point(499, 438);
            this.lblCorrectAns.Name = "lblCorrectAns";
            this.lblCorrectAns.Size = new System.Drawing.Size(118, 20);
            this.lblCorrectAns.TabIndex = 14;
            this.lblCorrectAns.Text = "Correct Answer";
            this.lblCorrectAns.Visible = false;
            // 
            // btnLaunchTable
            // 
            this.btnLaunchTable.Location = new System.Drawing.Point(907, 704);
            this.btnLaunchTable.Name = "btnLaunchTable";
            this.btnLaunchTable.Size = new System.Drawing.Size(149, 88);
            this.btnLaunchTable.TabIndex = 15;
            this.btnLaunchTable.Text = "Launch Table Form";
            this.btnLaunchTable.UseVisualStyleBackColor = true;
            this.btnLaunchTable.Click += new System.EventHandler(this.btnLaunchTable_Click);
            // 
            // btnEndSession
            // 
            this.btnEndSession.Location = new System.Drawing.Point(780, 334);
            this.btnEndSession.Name = "btnEndSession";
            this.btnEndSession.Size = new System.Drawing.Size(170, 68);
            this.btnEndSession.TabIndex = 16;
            this.btnEndSession.Text = "End Session";
            this.btnEndSession.UseVisualStyleBackColor = true;
            this.btnEndSession.Click += new System.EventHandler(this.btnEndSession_Click);
            // 
            // lblQuestionNumRight
            // 
            this.lblQuestionNumRight.AutoSize = true;
            this.lblQuestionNumRight.Location = new System.Drawing.Point(780, 268);
            this.lblQuestionNumRight.Name = "lblQuestionNumRight";
            this.lblQuestionNumRight.Size = new System.Drawing.Size(127, 20);
            this.lblQuestionNumRight.TabIndex = 17;
            this.lblQuestionNumRight.Text = "Questions Right:";
            this.lblQuestionNumRight.Visible = false;
            // 
            // lblQuestionNumAns
            // 
            this.lblQuestionNumAns.AutoSize = true;
            this.lblQuestionNumAns.Location = new System.Drawing.Point(780, 292);
            this.lblQuestionNumAns.Name = "lblQuestionNumAns";
            this.lblQuestionNumAns.Size = new System.Drawing.Size(160, 20);
            this.lblQuestionNumAns.TabIndex = 18;
            this.lblQuestionNumAns.Text = "Questions Answered:";
            this.lblQuestionNumAns.Visible = false;
            // 
            // btnClockMode
            // 
            this.btnClockMode.Location = new System.Drawing.Point(84, 485);
            this.btnClockMode.Name = "btnClockMode";
            this.btnClockMode.Size = new System.Drawing.Size(153, 60);
            this.btnClockMode.TabIndex = 19;
            this.btnClockMode.Text = "Against The Clock!";
            this.btnClockMode.UseVisualStyleBackColor = true;
            this.btnClockMode.Click += new System.EventHandler(this.btnClockMode_Click);
            // 
            // lblTimeLeft
            // 
            this.lblTimeLeft.AutoSize = true;
            this.lblTimeLeft.Location = new System.Drawing.Point(244, 503);
            this.lblTimeLeft.Name = "lblTimeLeft";
            this.lblTimeLeft.Size = new System.Drawing.Size(79, 20);
            this.lblTimeLeft.TabIndex = 20;
            this.lblTimeLeft.Text = "Time Left:";
            this.lblTimeLeft.Visible = false;
            // 
            // tmrClockMode
            // 
            this.tmrClockMode.Interval = 1000;
            this.tmrClockMode.Tick += new System.EventHandler(this.tmrClockMode_Tick);
            // 
            // frmArithmetic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 822);
            this.Controls.Add(this.lblTimeLeft);
            this.Controls.Add(this.btnClockMode);
            this.Controls.Add(this.lblQuestionNumAns);
            this.Controls.Add(this.lblQuestionNumRight);
            this.Controls.Add(this.btnEndSession);
            this.Controls.Add(this.btnLaunchTable);
            this.Controls.Add(this.lblCorrectAns);
            this.Controls.Add(this.txtCorrectAns);
            this.Controls.Add(this.btnAnswer);
            this.Controls.Add(this.btnGen);
            this.Controls.Add(this.lblAnswer);
            this.Controls.Add(this.lblQuestions);
            this.Controls.Add(this.txtAnswers);
            this.Controls.Add(this.lstVwQuestions);
            this.Controls.Add(this.rdBtnPosOnly);
            this.Controls.Add(this.rdBtnNegAndPos);
            this.Controls.Add(this.cmbBoxMagnitude);
            this.Controls.Add(this.lblMagReq);
            this.Controls.Add(this.lblOpReq);
            this.Controls.Add(this.cmbBoxOperations);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmArithmetic";
            this.Text = "Maths As Fun Learning Company Maths Quiz";
            this.Load += new System.EventHandler(this.frmArithmetic_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbBoxOperations;
        private System.Windows.Forms.Label lblOpReq;
        private System.Windows.Forms.Label lblMagReq;
        private System.Windows.Forms.ComboBox cmbBoxMagnitude;
        private System.Windows.Forms.RadioButton rdBtnNegAndPos;
        private System.Windows.Forms.RadioButton rdBtnPosOnly;
        private System.Windows.Forms.ListView lstVwQuestions;
        private System.Windows.Forms.TextBox txtAnswers;
        private System.Windows.Forms.Label lblQuestions;
        private System.Windows.Forms.Label lblAnswer;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.Button btnAnswer;
        private System.Windows.Forms.TextBox txtCorrectAns;
        private System.Windows.Forms.Label lblCorrectAns;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnLaunchTable;
        private System.Windows.Forms.Button btnEndSession;
        private System.Windows.Forms.Label lblQuestionNumRight;
        private System.Windows.Forms.Label lblQuestionNumAns;
        private System.Windows.Forms.Button btnClockMode;
        private System.Windows.Forms.Label lblTimeLeft;
        private System.Windows.Forms.Timer tmrClockMode;
    }
}